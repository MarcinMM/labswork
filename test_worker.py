import os
import json

import mock
import unittest
from mock import patch, Mock
from werkzeug.datastructures import MultiDict

import worker

NO_ARGS = MultiDict([])
ALL_ARGS = MultiDict(
    [
        ('inStock', 'true'),
        ('minReviewCount', '10'),
        ('maxReviewCount', '50'),
        ('minReviewRating', '4.0'),
        ('maxReviewRating', '5.0'),
        ('search', 'TV'),
        ('minPrice', '300'),
        ('maxPrice', '1000')
    ])

def _load_fixture(path):
    with open(os.path.join(os.path.dirname(__file__), path), 'r') as f:
        return json.load(f)


class MockResponse:
    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        return self.json_data


class MockInvalidResponse:
    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        raise TypeError

# FUTURE: More exhaustive tests.
class TestWorker(unittest.TestCase):

    @mock.patch('worker.requests.get')
    def test_init(self, mockRequestsGet):
        fixture = _load_fixture('fixtures/valid_results.json')
        mockReturn = MockResponse(fixture, 200)
        mockRequestsGet.return_value = mockReturn
        w = worker.Worker(NO_ARGS)
        results = w.fetch_remote_results()
        self.assertEqual(len(results), len(fixture["products"]))


    @mock.patch('worker.requests.get')
    def test_all_args(self, mockRequestsGet):
        fixture = _load_fixture('fixtures/valid_results.json')
        mockReturn = MockResponse(fixture, 200)
        mockRequestsGet.return_value = mockReturn
        w = worker.Worker(ALL_ARGS)
        results = w.fetch_remote_results()
        self.assertEqual(len(results), 1)


    @mock.patch('worker.requests.get')
    def test_exception(self, mockRequestsGet):
        fixture = _load_fixture('fixtures/valid_results.json')
        mockReturn = MockResponse(fixture, 400)
        mockRequestsGet.return_value = mockReturn
        with self.assertRaises(Exception):
            worker.Worker(NO_ARGS)


    @mock.patch('worker.requests.get')
    def test_invalid_results(self, mockRequestsGet):
        mockReturn = MockInvalidResponse('{}', 200)
        mockRequestsGet.return_value = mockReturn
        with self.assertRaises(Exception):
            worker.Worker(NO_ARGS)

if __name__ == '__main__':
    unittest.main()
