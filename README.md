## Fetch with filter project

0. Install Python 3.6, pip and pipenv.
1. pipenv shell
2. pip install -r _requirements.txt
3. FLASK_APP=worker.py flask run

## Testing

1. python test_worker.py
2. coverage run --source . test_worker.py
3. coverage report -m --omit test_worker.py

## Coverage

Name        Stmts   Miss  Cover   Missing
-----------------------------------------
worker.py      75      9    88%   41, 81, 83, 87, 89, 100-101, 111-112

## Resources

- https://docs.python-guide.org/dev/virtualenvs/
- https://mobile-tha-server.firebaseapp.com/
- https://bitbucket.org/MarcinMM/labswork/src/release/
