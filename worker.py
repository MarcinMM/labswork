import sys
import math
import random
import logging
import requests
import simplejson as json
import time

from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool

from flask import Flask
from flask import request, Response, jsonify, abort

SERVER_URL = 'https://mobile-tha-server.firebaseapp.com/walmartproducts/'
MAX_PAGE_SIZE = 30
logging.basicConfig(level=logging.INFO) # While in development; ERROR for prod.

class Worker():
    # Build URL list and ensure our server is available and producing JSON.
    def __init__(self, args):
        response = requests.get(SERVER_URL + '1/1')
        if response.status_code != 200:
            if response.status_code == 429:
                raise Exception("Catalog too busy, try again later.", response.status_code)
            else:
                raise Exception("Server failed to respond properly, status: ", response.status_code)
        try:
            responseJson = response.json()
        except Exception:
            raise Exception("Did not receive valid JSON response.")
        self.totalProducts = responseJson["totalProducts"]
        self.urls = []
        logging.info("Total products found: %s", self.totalProducts)
        # Construct list of URLs to retrieve catalog from.
        for i in range(1, math.ceil(responseJson["totalProducts"] / MAX_PAGE_SIZE) + 1):
            self.urls.append(SERVER_URL + str(i) + '/' + str(MAX_PAGE_SIZE))

        # Now parse our incoming args.
        # assuming inStock is the only special case that needs string comparison
        # FUTURE: Add detection of invalid filters (strings for numbers etc).
        if args.get('inStock', type=str) == 'true':
            inStock = True
        elif args.get('inStock', type=str) == 'false':
            inStock = False
        else:
            inStock = None
        self.filterArgs = {
            "search": args.get('search', default=None, type=str),
            "inStock": inStock,
            "minPrice": args.get('minPrice', default=0.0, type=float),
            "maxPrice": args.get('maxPrice', default=sys.maxsize, type=float),
            "minReviewRating": args.get('minReviewRating', default=0.0, type=float),
            "maxReviewRating": args.get('maxReviewRating', default=6.0, type=float),
            "minReviewCount": args.get('minReviewCount', default=0, type=int),
            "maxReviewCount": args.get('maxReviewCount', default=sys.maxsize, type=int)
        }
        # FUTURE: These should be metrics so we can analyze most requested args.
        # and build indexes/caching from them.
        logging.info("Arguments: %s", self.filterArgs)

    # Create, use and close connections needed for catalog retrieval.
    def fetch_remote_results(self):
        results = []
        # Collect our remote results
        pool = ThreadPool(4)
        response = pool.map(self._fetch_and_parse_products, self.urls)
        pool.close()
        pool.join()
        # Convert the result map to a serializable (JSONable) list
        for item in response:
            results.extend(item)
        logging.info("Results found: %s", len(results))
        return results

    # Perform filter operations on a per product basis.
    # FUTURE: Add detection of filter conflicts (ex max price smaller than min).
    def _filterProducts(self, product):
        if self.filterArgs["inStock"] is not None and self.filterArgs["inStock"] != product["inStock"]:
            return False
        if self.filterArgs["minReviewCount"] > product["reviewCount"]:
            return False
        if self.filterArgs["maxReviewCount"] < product["reviewCount"]:
            return False
        if self.filterArgs["minReviewRating"] > product["reviewRating"]:
            return False
        if self.filterArgs["maxReviewRating"] < product["reviewRating"]:
            return False
        if self.filterArgs["minPrice"] > float(product["price"].replace("$",'').replace(',','')):
            return False
        if self.filterArgs["maxPrice"] < float(product["price"].replace("$",'').replace(',','')):
            return False
        if self.filterArgs["search"] is not None and self.filterArgs["search"] not in product["productName"]:
            return False
        return True


    # Remove metadata and return filtered list of products.
    def _fetch_and_parse_products(self, url):
        time.sleep(self._jitter())
        logging.info("Now fetching url %s", url)
        try:
            response = requests.get(url)
            responseJson = response.json()
        except Exception as e:
            logging.info(response)
            raise Exception("Server returned invalid or malformed results.", response.status_code)
        filteredProductList = filter(self._filterProducts, responseJson["products"])
        return filteredProductList


    # Stagger and delay our multithreaded fetches to avoid overloading server.
    # This could be tuned with research or knowing server perf, or replaced
    # altogether with backoff, although ideally we'd know expected TPS and
    # could avoid throttles entirely.
    def _jitter(self):
        offset = random.randint(-3,3)
        base = 0.1
        jitter = base + offset * 0.015
        return jitter


app = Flask(__name__)

# Our single request route. All args captured in request.
@app.route('/', methods=['GET'])
def fetch_results():
    try:
        worker = Worker(request.args)
        return jsonify(worker.fetch_remote_results())
    except Exception as e:
        logging.info("Caught exception: %s", e)
        if e.args[1] == 429:
            abort(429)
        else:
            abort(500)
